﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FileHosting.Models
{
    [Table("UserEntity")]
    public class UserModel
    {
        [Key]
        [Column]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [StringLength(256)]
        [Required]
        public string Login { get; set; }

        [StringLength(256)]
        [Required]
        public string Email { get; set; }

        [StringLength(512)]
        public string FullName { get; set; }

        [DefaultValue(true)]
        public bool IsActive { get; set; }

        [NotMapped]
        public string Password {get;set;}

        public virtual ICollection<FileModel> Files { get; set; }
    }
}