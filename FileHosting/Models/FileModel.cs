﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FileHosting.Models
{
    [Table("Files")]
    public class FileModel
    {
        [Key]
        [Column]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int FileId { get; set; }

        [ForeignKey("UserModel")]
        public int UserId { get; set; }

        [Required]
        public string VisFileName { get; set; }

        [Required]
        public string Path { get; set; }

        public virtual UserModel UserModel { get; set; }
    }
}