﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;
using WebMatrix.Data;
using System.Web.Security;
using FileHosting.Providers;

namespace FileHosting.App_Start
{
    public class WebSecurityConfig
    {
        public static void Config()
        {
            if (!WebSecurity.Initialized)
            {
                using (var db = new FileHostingContext()) { };
                WebSecurity.InitializeDatabaseConnection("FileHostingContext", "UserEntity", "UserId", "Login", autoCreateTables: true);
            }

            if (!Roles.RoleExists("users"))
            {
                Roles.CreateRole("users");
            }
        }
    }
}