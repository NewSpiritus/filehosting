﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace FileHosting.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/jquery/js")
                .Include("~/Content/Scripts/jquery*"));

            bundles.Add(new ScriptBundle("~/bootstrap/js")
                .Include("~/Content/Scripts/bootstrap*"));

            bundles.Add(new ScriptBundle("~/filehosting/js")
                .Include("~/Content/Scripts/filehosting.js"));

            bundles.Add(new ScriptBundle("~/validator/js")
                .Include("~/Content/Scripts/validator.js"));

            bundles.Add(new StyleBundle("~/jquery/css")
                .Include("~/Content/Styles/jquery*"));

            bundles.Add(new StyleBundle("~/bootstrap/css")
                    .Include("~/Content/Styles/bootstrap*"));

            bundles.Add(new ScriptBundle("~/filehosting/css")
                .Include("~/Content/Styles/filehosting.css")
                .Include("~/Content/Styles/login.css"));
        }
    }
}