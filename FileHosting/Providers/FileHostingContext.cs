﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using FileHosting.Models;

namespace FileHosting.Providers
{
    public class FileHostingContext : DbContext
    {
        public FileHostingContext()
            : base("FileHostingContext")
        {
            Database.Initialize(false);
        }

        public DbSet<UserModel> UserEntities { get; set; }
        public DbSet<FileModel> Files { get; set; }
    }
}