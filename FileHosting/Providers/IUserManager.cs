﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHosting.Models;

namespace FileHosting.Providers
{
    public interface IUserManager
    {
        UserModel GetUser(string userName);
        UserModel GetUser(int userId);
        void AddUser(UserModel user);
        void EditUser(UserModel user);
        IEnumerable<UserModel> UserList();
        bool LoginUser(string userName, string password);
        void ResetPassword(UserModel user, string newPassword);
        IQueryable<UserModel> Users { get; }
    }
}
