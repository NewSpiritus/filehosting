﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;
using System.Web.Security;
using FileHosting.Models;

namespace FileHosting.Providers
{
    public class MembershipUserStorage : IUserManager
    {
        public UserModel GetUser(string userName)
        { 
            using (var db = new FileHostingContext())
            {
                return db.UserEntities.FirstOrDefault(u => u.Login.Equals(userName, StringComparison.OrdinalIgnoreCase));
            }
        }

        public UserModel GetUser(int userId)
        {
            using (var db = new FileHostingContext())
            {
                return db.UserEntities.FirstOrDefault(u => u.UserId == userId);
            }
        }

        public void AddUser(UserModel user)
        {
            if (WebSecurity.UserExists(user.Login))
                throw new Exception(string.Format("\"{0}\" is already exists", user.Login));

            WebSecurity.CreateUserAndAccount(user.Login, user.Password,
                new
                {
                    Email = user.Email,
                    FullName = user.FullName,
                    IsActive = user.IsActive
                }
            );
            Roles.AddUserToRole(user.Login, "Users");
        }

        public void EditUser(UserModel user)
        {
                using (var db = new FileHostingContext())
                {
                    var userEntity = db.UserEntities.FirstOrDefault(u => u.Login.Equals(user.Login));
                    if (userEntity == null)
                        throw new Exception(string.Format("\"{0}\" doesnt exist", user.Login));

                    userEntity.Login = user.Login;
                    userEntity.FullName = user.FullName;
                    userEntity.IsActive = user.IsActive;
                    userEntity.Email = user.Email;
                    db.SaveChanges();
                }
        }

        public IEnumerable<UserModel> UserList()
        {
            using (var db = new FileHostingContext())
            {
                return db.UserEntities.ToArray();
            }
        }

        public bool LoginUser(string userName, string password)
        {
            return WebSecurity.Login(userName, password, false);
        }

        public void ResetPassword(UserModel user, string newPassword)
        {
            string token = WebSecurity.GeneratePasswordResetToken(user.Login);
            WebSecurity.ResetPassword(token, newPassword);
        }

        public IQueryable<UserModel> Users
        {
            get
            {
                using (var db = new FileHostingContext())
                {
                    return db.UserEntities;
                }
            }
        }
    }
}