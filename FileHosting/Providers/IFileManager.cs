﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHosting.Models;

namespace FileHosting.Providers
{
    public interface IFileManager
    {
        void AddFile(FileModel file);
        FileModel GetFile(int fileId);
        FileModel GetFile(string path);
        void DeleteFile(int fileId);
        ICollection<FileModel> FilesList(int userId);
        ICollection<FileModel> FilesList(string userName);
    }
}
