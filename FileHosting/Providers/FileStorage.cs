﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FileHosting.Models;

namespace FileHosting.Providers
{
    public class FileStorage : IFileManager
    {
        public void AddFile(FileModel file)
        {
            using (var db = new FileHostingContext())
            {
                db.Files.Add(file);
                db.SaveChanges();
            }
        }
        public void EditFile(FileModel file)
        {
            using (var db = new FileHostingContext())
            {
                var foundFile = db.Files.FirstOrDefault(f => f.FileId == file.FileId);
                if (foundFile == null)
                    throw new Exception(string.Format("File with id={0} doesnt exist", file.FileId));

                foundFile.FileId = file.FileId;
                foundFile.Path = file.Path;
                foundFile.UserId = file.UserId;
                db.SaveChanges();
            }
        }

        public FileModel GetFile(int fileId)
        {
            using (var db = new FileHostingContext())
            {
                return db.Files.FirstOrDefault(f => f.FileId == fileId);
            }
        }

        public FileModel GetFile(string path)
        {
            using (var db = new FileHostingContext())
            {
                return db.Files.FirstOrDefault(f => f.Path.Equals(path, StringComparison.OrdinalIgnoreCase));
            }
        }

        public void DeleteFile(int fileId)
        {
            using (var db = new FileHostingContext())
            {
                var foundFile = db.Files.FirstOrDefault(f => f.FileId == fileId);
                if (foundFile == null)
                    throw new Exception(string.Format("File with id={0} doesnt exist", fileId));

                db.Files.Remove(foundFile);
                db.SaveChanges();
            }
        }

        public ICollection<FileModel> FilesList(int userId)
        { 
            using (var db = new FileHostingContext())
            {
                return db.Files.Where(f => f.UserId == userId).ToList();
            }
        }

        public ICollection<FileModel> FilesList(string userName)
        {
            using (var db = new FileHostingContext())
            {
                var user = db.UserEntities.FirstOrDefault(u => u.Login.Equals(userName, StringComparison.OrdinalIgnoreCase));
                return db.Files.Where(f => f.UserId == user.UserId).ToList();
            }
        }
    }
}