﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FileHosting.ViewModels
{
    public class BaseUserViewModel
    {
        [Required(ErrorMessage = "Login name is required")]
        [StringLength(16, ErrorMessage = "Ivalid login length", MinimumLength = 4)]
        public string Login { get; set; }
        [Required(ErrorMessage = "Email cant be empty")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string FullName { get; set; }
    }
}