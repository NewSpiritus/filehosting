﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FileHosting.ViewModels
{
    public class ResetPasswordViewModel
    {
        public string Login { get; set; }
        [Required(ErrorMessage="Password cant be empty")]
        [StringLength(1024, ErrorMessage = "Ivalid login length", MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }
        [Required(ErrorMessage = "Password cant be empty")]
        [StringLength(1024, ErrorMessage = "Ivalid login length", MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "Confirm cant be empty")]
        [Compare("NewPassword", ErrorMessage = "Password and confirm should be equal")]
        [DataType(DataType.Password)]
        public string ConfirmNewPassword { get; set; }
    }
}