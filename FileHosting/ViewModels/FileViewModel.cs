﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileHosting.ViewModels
{
    public class FileViewModel
    {
        public int FileId { get; set; }
        public int UserId { get; set; }
        public string Path { get; set; }
        public string VisFileName { get; set; }
    }
}