﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FileHosting.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Login name is reqiured")]
        [StringLength(16, ErrorMessage="Login has invalid length (maximum 16 characters)")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Password is reqiured")]
        [StringLength(1024, ErrorMessage = "Password has invalid length (maximum 1024 characters)")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}