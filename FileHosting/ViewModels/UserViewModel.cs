﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using FileHosting.ViewModels;

namespace FileHosting.ViewModels
{
    public class UserViewModel : BaseUserViewModel
    {
        [Required(ErrorMessage="Password cant be empty")]
        [StringLength(1024, ErrorMessage = "Ivalid login length", MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Confirm cant be empty")]
        [Compare("Password", ErrorMessage="Password and confirm should be equal")]
        [DataType(DataType.Password)]
        public string Confirm { get; set; }
    }
}