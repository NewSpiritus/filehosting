﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FileHosting.Models;
using System.IO;
using WebMatrix.WebData;

namespace FileHosting.Controllers
{
    public class DefaultController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {            
            ViewBag.files = MvcApplication.FileManager.FilesList(WebSecurity.CurrentUserId);
            return View("Home");
        }
    }
}
