﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using FileHosting.ViewModels;
using WebMatrix.WebData;
using FileHosting.Services;

namespace FileHosting.Controllers
{

    public class FilesController : Controller
    {
        IFilesService _service;
        public FilesController()
        {
            _service = new FilesService();
        }

        [Authorize]
        [HttpPost]
        public JsonResult Upload()
        {
            HttpPostedFileBase file = Request.Files[0] as HttpPostedFileBase;
            FileViewModel fileVM = _service.Upload(Server.MapPath("~/App_Data"), file);
            if (fileVM == null)
                return Json(new { success = false });

            return Json(new { success = true,
                              name = file.FileName,
                              size = string.Format("{0:F2} KB", file.ContentLength / 1024),
                              date = DateTime.Now.ToString("dd.MM.yy HH:mm"),
                              fileId = fileVM.FileId
            });
        }

        [Authorize]
        [HttpGet]
        public JsonResult Delete(int? id)
        {
            return Json(new { success = _service.Delete(Server.MapPath("~/App_Data"), id) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public FileResult Download(int? id)
        {
            FileViewModel file = _service.GetFileViewModel(id);

            string fullFileName = Path.Combine(new string[] { Server.MapPath("~/App_Data"), file.Path });
            if (!System.IO.File.Exists(fullFileName))
                return null;

            byte[] fileBytes = System.IO.File.ReadAllBytes(fullFileName);
            if (!System.IO.File.Exists(fullFileName))
                return null;

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, file.VisFileName);
        }
    }
}