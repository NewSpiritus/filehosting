﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FileHosting.Models;
using WebMatrix.WebData;
using FileHosting.ViewModels;
using FileHosting.Services;

namespace FileHosting.Controllers
{
    public class AccountController : Controller
    {
        IAccountService _service;

        public AccountController() 
            : base()
        {
            _service = new AccountService();
        }

        [HttpGet]
        public ActionResult Login()
        {
            if (WebSecurity.IsAuthenticated)
                return RedirectToAction("Index", "Default");
            return View("Login");
        }

        [HttpGet]
        public ActionResult Logout()
        {
            WebSecurity.Logout();
            return RedirectToAction("Index", "Default");
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel user)
        {
            if (ModelState.IsValid && _service.Login(user))
                return RedirectToAction("Index", "Default");
            return View(user);
        }

        [HttpGet]
        public ActionResult SignUp(int? id)
        {
            var user = _service.GetUserViewModel(id);
            return View("SignUp", user);
        }

        [HttpPost]
        public JsonResult SignUp(UserViewModel user)
        {
            if (!ModelState.IsValid)
                return Json(new { success = false  });

            bool res = _service.SignUp(user);
            return Json(new { success = res  });
        }

        [HttpGet]
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (WebSecurity.CurrentUserId != id) 
                return RedirectToAction("EditProfile", new { id = WebSecurity.CurrentUserId});
            var user = _service.GetBaseUserViewModel(id);
            return View("Profile", user);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Edit(BaseUserViewModel user)
        {
            if (!ModelState.IsValid)
                return Json( new {success = false});

            bool res = _service.Edit(user);
            return Json(new { success = res });
        }

        [HttpPost]
        [Authorize]
        public JsonResult ResetPassword(ResetPasswordViewModel user)
        {
            if (!ModelState.IsValid)
                return Json(new { success = false });

            user.Login = User.Identity.Name;
            bool res = _service.ResetPassword(user);
            return Json(new { success = res });
        }
    }
}