function showAlert(msg) {
    $("#msg").text(msg);
    $("#alert").fadeTo(4000, 500).slideUp(500, function () {
        $("#alert").slideUp(500);
    });
}

function showError(msg) {
    $("#alert").removeClass("alert-success").addClass("alert-danger");
    $("#status").text("Error!");
    showAlert(msg);
}

function showSuccess(msg) {
    $("#alert").removeClass("alert-danger").addClass("alert-success");
    $("#status").text("Success!");
    showAlert(msg);
}

function addTableRow(table, data) {
    console.log(table);
    table.append('<tr></tr>');

    var lastRow = table.children('tbody').children().last();

    lastRow.append('<td><span class="glyphicon glyphicon-file"></span>' + data.result.name + '</td>');
    lastRow.append('<td></td>');
    var actionsCol = lastRow.children().last();

    actionsCol.append('<button type="button" class="btn btn-sm btn-success btnDownload" id=' + data.result.fileId + '><i class="glyphicon glyphicon-download-alt"></i></button> ');
    actionsCol.append('<button type="button" class="btn btn-sm btn-danger btnDelete" id=' + data.result.fileId + '><i class="glyphicon glyphicon-trash"></i></button>');
    lastRow.append('<td>' + data.result.size + '</td><td>' + data.result.date + '</td>');
}

$(document).ready(function () {

    $('#fileupload').fileupload({
        dropZone: $('#drop-zone'),
        dataType: 'json',
        url: 'Files/Upload',
        autoUpload: true,
        done: function (e, data) {
            $('.progress').css('display', 'none');
            if (data.result.success) {
                addTableRow($('table'), data);
                showSuccess("Files were uploaded successfully");
            } else {
                showError("Upload failed")
            }
        },
        fail: function (data) {
            $('.progress').css('display', 'none');
            showError("Upload failed")
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.total != 0 ? (data.loaded / data.total * 100) : 100, 10);
        $('.progress .progress-bar').css('width', progress + '%');
    }).on('fileuploadsend', function (e, data) {
        $('.progress').css('display', '');
    });


    $('#drop-zone').ondragover = function () {
        $('#drop-zone').addClass("drop");
    }

    $('table').on('click', '.btnDelete', function () {
        var id = $(this).attr("id");
        var row = $(this).parents('tr');
        $.ajax({
            url: 'Files/Delete/' + id,
            type: "GET",
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    row.remove();
                } else {
                    console.log("failed");
                    showError('Unable to delete file');
                }
            },
            error: function () {
                showError('Unable to delete file');
            }
        });
    });

    $('table').on('click', '.btnDownload', function () {
        var id = $(this).attr("id");
        window.location = 'Files/Download/' + id;
    });

    $('#editProfile').submit(function (event) {
        event.preventDefault();
        var login = $('#login');
        var email = $('#email');
        var fullName = $('#fullName');
        $.ajax({
            type: "POST",
            url: "Edit",
            dataType: "json",
            data: {
                Login: login.val(),
                Email: email.val(),
                FullName: fullName.val()
            },
            success: function (data) {
                if (data.success)
                    showSuccess("Profile was edited successfully");
                else
                    showError("Profile editing failed");
            },
            error: function () {
                showError("Profile editing failed");
            }
        });
    });

    $('#resetPassword').submit(function (event) {
        event.preventDefault();
        var oldPwd = $('#old');
        var newPwd = $('#new');
        var confirm = $('#confirm');
        $.ajax({
            type: "POST",
            url: "../ResetPassword",
            dataType: "json",
            data: {
                OldPassword: oldPwd.val(),
                NewPassword: newPwd.val(),
                ConfirmNewPassword: confirm.val()
            },
            success: function (data) {
                if (data.success)
                    showSuccess("Password was reseted successfully");
                else
                    showError("Password reset failed");
            },
            error: function () {
                showError("Password reset failed");
            }
        });
    });

    $('#signUp').submit(function (event) {
        event.preventDefault();
        var login = $('#login');
        var password = $('#password');
        var confirm = $('#confirm');
        var email = $('#email');
        var fullName = $('#fullName');
        $.ajax({
            type: "POST",
            url: "/FileHosting/Account/Signup",
            dataType: "json",
            data: {
                Login: login.val(),
                Password: password.val(),
                Confirm: confirm.val(),
                Email: email.val(),
                FullName: fullName.val()
            },
            success: function (data) {
                if (data.success)
                    showSuccess("User was registered successfully.");
                else
                    showError("Registrtion failed. " + data.message);
            },
            error: function () {
                showError("Registration failed. " + data.message);
            }
        });
    });
});

$(document).bind('dragleave', function (e) {
    $('#drop-zone').removeClass('drop');
});

$(document).bind('dragover', function (e) {
    $('#drop-zone').addClass('drop');
});

$(document).bind('drop', function (e) {
    $('#drop-zone').removeClass('drop');
});