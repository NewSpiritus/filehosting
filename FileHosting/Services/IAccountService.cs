﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHosting.ViewModels;
using FileHosting.Models;

namespace FileHosting.Services
{
    interface IAccountService
    {
        UserViewModel GetUserViewModel(int? id);
        bool SignUp(UserViewModel user);
        BaseUserViewModel GetBaseUserViewModel(int? id);
        bool Edit(BaseUserViewModel user);
        bool ResetPassword(ResetPasswordViewModel user);
        bool Login(LoginViewModel user);
    }
}
