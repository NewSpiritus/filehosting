﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FileHosting.Models;
using WebMatrix.WebData;
using FileHosting.ViewModels;

namespace FileHosting.Services
{
    public class AccountService : IAccountService
    {
        public UserViewModel GetUserViewModel(int? id) 
        {
            UserViewModel user = new UserViewModel();
            if (id != null)
            {
                var userModel = MvcApplication.UserManager.GetUser((int)id);
                user = new UserViewModel
                {
                    Email = userModel.Email,
                    Login = userModel.Login,
                    FullName = userModel.FullName
                };
            }
            return user;
        }

        public bool SignUp(UserViewModel user)
        {
            if (user == null)
                return false;

            try
            {
                MvcApplication.UserManager.AddUser(new UserModel
                {
                    Login = user.Login,
                    Password = user.Password,
                    Email = user.Email,
                    FullName = user.FullName,
                    IsActive = true
                });
                return true;
            }
            catch
            {
                return false;
            }
        }

        public BaseUserViewModel GetBaseUserViewModel(int? id)
        {
            BaseUserViewModel user = new BaseUserViewModel();
            if (id != null)
            { 
                var userModel = MvcApplication.UserManager.GetUser(WebSecurity.CurrentUserId);
                user = new BaseUserViewModel
                {
                    Email = userModel.Email,
                    Login = userModel.Login,
                    FullName = userModel.FullName
                };
            }
            return user;
        }

        public bool Edit(BaseUserViewModel user)
        {
            if (user == null)
                return false;

            try
            {
                MvcApplication.UserManager.EditUser(new UserModel
                {
                    Email = user.Email,
                    Login = user.Login,
                    FullName = user.FullName
                });
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool ResetPassword(ResetPasswordViewModel user)
        {
            if (user == null)
                return false;

            try
            {
                MvcApplication.UserManager.ResetPassword(new UserModel
                {
                    Login = user.Login,
                    Password = user.OldPassword
                }, user.NewPassword);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Login(LoginViewModel user)
        {
            if (!MvcApplication.UserManager.LoginUser(user.Login, user.Password))
                return false;

            return MvcApplication.UserManager.GetUser(user.Login) != null;
        }
    }
}