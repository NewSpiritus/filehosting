﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;
using System.IO;
using FileHosting.Models;
using FileHosting.ViewModels;
using System.Security.Cryptography;
using System.Text;

namespace FileHosting.Services
{
    public static class CryptUtils
    {
        public static string generateMD5(string s)
        {
            MD5 md5 = MD5.Create();
            byte[] hashBytes = md5.ComputeHash(Encoding.UTF8.GetBytes(s));

            var sb = new StringBuilder();
            foreach (byte b in hashBytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            return sb.ToString();
        }
    }

    public static class FileUtils
    {
        public static string GeneratePath(string visFileName)
        {
            var fileName = CryptUtils.generateMD5(string.Format("{0}{1}", DateTime.Now.ToString("HHmmssff"), Path.GetFileName(visFileName)));
            return Path.Combine(new string[] { DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), DateTime.Now.Day.ToString(), fileName });
        }

        public static void SaveFile(HttpPostedFileBase file, string fullFileName)
        {
            string fullTargetPath = Directory.GetParent(fullFileName).FullName;
            if (!Directory.Exists(fullTargetPath))
                Directory.CreateDirectory(fullTargetPath);

            file.SaveAs(fullFileName);
        }
    }
    public class FilesService : IFilesService
    {
        public FileViewModel Upload(string rootPath, HttpPostedFileBase file)
        {
            if (file == null || file.ContentLength == 0)
                return null;


            string path = FileUtils.GeneratePath(file.FileName);

            var user = MvcApplication.UserManager.GetUser(WebSecurity.CurrentUserId);
            if (user == null)
                return null;

            FileModel fileEntity;
            try
            {
                MvcApplication.FileManager.AddFile(new FileModel()
                {
                    Path = path,
                    VisFileName = file.FileName,
                    UserId = user.UserId
                });
                fileEntity = MvcApplication.FileManager.GetFile(path);
                FileUtils.SaveFile(file, Path.Combine(new string[] { rootPath, path }));
            }
            catch
            {
                return null;
            }
            return new FileViewModel() { FileId = fileEntity.FileId };
        }
        public bool Delete(string rootPath, int? id)
        {
            if (id == null)
                return false;

            FileModel fileEntity = MvcApplication.FileManager.GetFile((int)id);
            if (fileEntity == null || fileEntity.UserId != WebSecurity.CurrentUserId)
                return false;


            MvcApplication.FileManager.DeleteFile((int)id);

            string fullFileName = Path.Combine(new string[] { rootPath, fileEntity.Path });
            if (!System.IO.File.Exists(fullFileName))
                return false;

            try
            {
                System.IO.File.Delete(fullFileName);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public FileViewModel GetFileViewModel(int? id)
        {
            if (id == null)
                return null;

            FileModel fileEntity = MvcApplication.FileManager.GetFile((int)id);
            if (fileEntity == null)
                return null;

            if (fileEntity.UserId != WebSecurity.CurrentUserId)
                return null;

            return new FileViewModel() { Path = fileEntity.Path, VisFileName = fileEntity.VisFileName };
        }
    }
}