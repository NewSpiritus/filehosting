﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHosting.ViewModels;
using System.Web;

namespace FileHosting.Services
{
    interface IFilesService
    {
        FileViewModel Upload(string rootPath, HttpPostedFileBase file);
        bool Delete(string rootPath, int? id);
        FileViewModel GetFileViewModel(int? id);
    }
}