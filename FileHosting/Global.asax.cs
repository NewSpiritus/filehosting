﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using FileHosting.Providers;
using FileHosting.App_Start;
using System.Web.Optimization;

namespace FileHosting
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        private static IUserManager userManager;
        private static IFileManager fileManager;
        public static IUserManager UserManager
        {
            get
            {
                if (userManager == null)
                    userManager = new MembershipUserStorage();
                return userManager;
            }
        }

        public static IFileManager FileManager
        {
            get
            {
                if (fileManager == null)
                    fileManager = new FileStorage();
                return fileManager;
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            WebSecurityConfig.Config();
        }
    }
}