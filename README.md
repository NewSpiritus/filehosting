# File Hosting #

Allows user to upload and store files

### What technologies and plugins are used? ###

Back-End: 

* ASP.NET MVC 4
* Entity Framework
* Simple Membership
	
Front-End: 

* Bootstrap
* https://github.com/1000hz/bootstrap-validator
* https://github.com/blueimp/jQuery-File-Upload

### How do I get set up? ###

* Specify connection string in Web.Config
* Build project